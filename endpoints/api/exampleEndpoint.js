var anArray = []

module.exports = {
  GET() {
    return anArray
  },
  POST({ req, res }) {
    var data = ''
    req.on('data', chunk => {
      data += chunk
    })
    req.on('end', () => {
      anArray.push(req.body.toString())
      return anArray
    })
  },
  PUT() {
    var data = ''
    req.on('data', chunk => {
      data += chunk
    })
    req.on('end', () => {
      anArray = [req.body.toString()]
      return anArray
    })
  }
}
