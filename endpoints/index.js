var endpoints = {
  '/api/endpoint': { path: './api/exampleEndpoint.js' }
}

var missingEndpoints = []

// Assemble endpoints from api files and endpoints object
for (let e in endpoints) {
  let endpoint = endpoints[e]
  let methods = null
  // Catch missing endpoints
  try {
    methods = require(endpoint.path)
  } catch () {
    missingEndpoints.push(endpoint)
  }
  if (methods == null) {
    for (let m in methods) {
      endpoint[m] = methods[m]
    }
  }
}
// Init log loaded endpoints
console.log(`Loaded ${endpoints.length} endpoints:`)
console.log(JSON.stringify(Object.keys(endpoints), null, 2))

if (missingEndpoints.length) {
  console.log('Failed to load:')
  console.log(JSON.stringify(missingEndpoints, null, 2))
}

module.exports = endpoints
