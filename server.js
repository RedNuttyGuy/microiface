const http = require('http')
const endpoints = require('./endpoints')
const nodeUrl = require('url')

module.exports = {
  create() {
    return http.createServer(async (req, res) => {
      // Set timeout to 10s
      req.setTimeout(0)

      let { url, method } = req

      url = nodeUrl.parse(url)
      method = method.toUpperCase()

      if (endpoints[url.pathname]) {
        const endpoint = endpoints[url.pathname]
        var result = ''
        if (endpoint[method] != null) {
          console.log(`${method} ${endpoint}`)
          try {
            result = await endpoint[method]({ req, res })
            if (typeof(result) === 'object' && !Buffer.isBuffer(result)) {
              try {
                result = JSON.stringify(result)
              } catch (error) {
                console.error(error)
              }
            }
            res.end(result)
          } catch (error) {
            console.error({ endpoint: url.pathname, error })
          }
        }
      } else {
        res.statusCode = 404
        res.end()
        console.warn({ endpoint: url.pathname, error: "Endpoint not found" })
      }
    })
  }
}
